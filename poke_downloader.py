import tkinter
from requests import get
from multiprocessing.dummy import Pool
from pokemo_download import donwload_imgs
from threading import Thread

class PokeDownloader():
    def __init__(self):
        self.janela = tkinter.Tk()
        self.janela.geometry("400x250")
        self.janela["bg"] = "black"
        self.janela.title("Pokemon image Downloader")

        self.frame1 = tkinter.Frame(self.janela, bg="black")
        self.frame2 = tkinter.Frame(self.janela, bg="black")

        self.logo = tkinter.PhotoImage(file="poke.png")

        self.lb_img = tkinter.Label(self.frame1, bg="black", image=self.logo)
        self.log = tkinter.Label(self.frame1, bg="black", fg="white")
        self.bt_ok = tkinter.Button(self.frame2, text="OK", width=5, height=2, bg="green", fg="white", command=self.update)
        self.et_n = tkinter.Entry(self.frame2, bg="green", fg="white")

        self.lb_img.pack()
        self.log.pack()
        self.et_n.pack()
        self.bt_ok.pack()
        self.frame1.pack(side="top")
        self.frame2.pack(side="bottom")

        self.janela.mainloop()

    def download(self):
        saida = donwload_imgs(int(self.et_n.get()))
        self.log["text"] = saida

    def update(self):
        self.log["text"] = "Baixando..."
        t1 = Thread(target=self.download, name="down", daemon=True)
        t1.start()

if __name__ == "__main__":
    PokeDownloader()

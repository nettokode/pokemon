from requests import get
from multiprocessing.dummy import Pool

def get_url(pokemon: str) -> str:
    print("Pengando url...")
    query_url = f"https://pokeapi.co/api/v2/pokemon/{pokemon}/"
    return query_url

def get_poke_img(url: str) -> str:
    print("Fazendo download da imagen...")
    with get(url) as r:
        with get(r.json()["sprites"]["front_default"]) as img:
            nome = r.json()["name"]
            return nome, img.content

def save_img(args: tuple) -> None:
    print("Salvando imagen...")
    name, img = args
    with open(f"imagens/{name}.png", "wb") as f:
        f.write(img)

def pipeline(*funcs: list) -> object:
    def interna(argumento):
        estado = argumento
        for func in funcs:
            estado = func(estado)
    return interna

alvo = pipeline(get_url, get_poke_img, save_img)

# threads = Pool(100)

def donwload_imgs(nums: int) -> None:
    with Pool(5) as threads:
        threads.map(alvo, range(1, nums+1))
    return ("Terminei de baixar!!")
